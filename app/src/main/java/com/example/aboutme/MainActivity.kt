package com.example.aboutme

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val name = Name("John")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*setContentView(R.layout.activity_main)*/

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.mName = name

        /* acessing via data binding */
        binding.btnDone.setOnClickListener {
            addNickname(it)
        }

        /* acessing without data binding
        btnDone.setOnClickListener {
            addNickname(it)
        }*/
    }

    private fun addNickname(view: View) {
        binding.apply {
            mName?.nickname = nickname.text.toString()
            invalidateAll()
            nickname.visibility = View.GONE
            btnDone.visibility = View.GONE
            nicknameText.visibility = View.VISIBLE
        }

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
